---
name: "JingLing JingPad A1/C1"
deviceType: "tablet"
buyLink: "https://en.jingos.com/jingpad-a1/"
subforum: "107/jingpad-a1"

deviceInfo:
  - id: "cpu"
    value: "UNISOC Tiger T7510"
  - id: "chipset"
    value: "8-cores chipset with 4x Cortex-A75 cores clocked at 2.0GHz and 4x Cortex-A55 cores clocked at 1.8GHz"
  - id: "gpu"
    value: "PowerVR Rogue GM9446"
  - id: "rom"
    value: "256 GB"
  - id: "ram"
    value: "8 GB"
  - id: "battery"
    value: "8000 mAh"
  - id: "display"
    value: '11" Multi-Touch display'
  - id: "rearCamera"
    value: "Single 15.9MP″, LED Flash"
  - id: "frontCamera"
    value: "Single 8MP"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "243mm x 178mm x 11.2mm"
  - id: "weight"
    value: "'less than 500g'"
sources:
  portType: "community"
  portPath: "android10"
  deviceGroup: "jingpad-a1"
  deviceSource: "jingpad-a1"
  kernelSource: "kernel-jingpad-a1"
communityHelp:
  - name: "Telegram chat"
    link: "https://t.me/UT_on_JingPad"
contributors:
  - name: "fredldotme"
    forum: "https://forums.ubports.com/user/fredldotme"
    photo: "https://forums.ubports.com/assets/uploads/profile/uid-2070/2070-profileavatar.png"
  - name: "TheKit"
    forum: https://forums.ubports.com/user/thekit
---
