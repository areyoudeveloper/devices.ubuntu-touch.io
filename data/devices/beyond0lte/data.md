---
variantOf: "beyond1lte"
name: "Samsung Galaxy S10e (Exynos)"
description: "Samsung's flagship for 2019. Features a Exynos 9820 SOC and a holepunch display."

deviceInfo:
  - id: "rom"
    value: "128/256 GB"
  - id: "ram"
    value: "6/8 GB"
  - id: "battery"
    value: "3100 mAh"
  - id: "display"
    value: "1080x2280 pixels, 5.8 in"
  - id: "dimensions"
    value: "142.2 x 69.9 x 7.9 mm (5.60 x 2.75 x 0.31 in)"
  - id: "weight"
    value: "150g"
---
