---
name: "Xiaomi Mi 6"
deviceType: "phone"
image: "https://wiki.lineageos.org/images/devices/sagit.png"
buyLink: "https://s.taobao.com/search?q=小米6"
description: "Switch your Xiaomi Mi 6, as your open source daily driver OS."
subforum: "91/xiaomi"
price:
  avg: 350
  currency: "CNY"
  currencySymbol: "¥"

deviceInfo:
  - id: "arch"
    value: "Arm64"
  - id: "cpu"
    value: "Quad-core 2.45 GHz Kryo 280 & Quad-core 1.9 GHz Kryo 280"
  - id: "chipset"
    value: "Qualcomm MSM8998 Snapdragon 835"
  - id: "gpu"
    value: "Adreno 540"
  - id: "rom"
    value: "64/128GB"
  - id: "ram"
    value: "4/6GB"
  - id: "android"
    value: "Android 7.1 Nougat"
  - id: "battery"
    value: "Non-removable Li-Po 3350 mAh (QC 3.0)"
  - id: "display"
    value: "1080 x 1920 pixels, 5.15 inches (~428 ppi pixel density)"
  - id: "rearCamera"
    value: "Dual 12 MP, f/2.0, phase detection autofocus, dual-LED (dual tone) flash"
  - id: "frontCamera"
    value: "5 MP (No flash)"
  - id: "dimensions"
    value: "70,49 x 145,17 x 7,45 (length x width x height, mm.)"
  - id: "weight"
    value: "168g"
  - id: "releaseDate"
    value: "September 2021"

contributors:
  - name: "Verevka"
    photo: "https://github.com/UbuntuTouch-sagit/ubuntu-touch-sagit/releases/download/v1.1/verevka.jpeg"
    forum: "https://forums.ubports.com/user/verevka"
    role: "Maintainer"
    renewals:
      - "2021-09-25"
  - name: "kuailexs"
    photo: "https://forums.ubports.com/assets/uploads/profile/uid-278/278-profileavatar.png"
    forum: "https://forums.ubports.com/user/kuailexs"
    role: "Maintainer"
    renewals:
      - "2024-11-04"

sources:
  portType: "community"
  portPath: "android9"
  deviceGroup: "xiaomi-mi-6"
  deviceSource: "xiaomi-sagit"
  kernelSource: "android_kernel_xiaomi_msm8998"

externalLinks:
  - name: "Forum Post"
    link: "https://forums.ubports.com/topic/4671/ubuntu-touch-halium9-0-xiaomi-mi6-sagit"

seo:
  description: "Switch your Xiaomi Mi 6, as your open source daily driver OS."
  keywords: "Ubuntu Touch, Xiaomi Mi 6, sagit, Linux on Mobile"
---
