---
name: "Pinebook"
deviceType: "laptop"
buyLink: "https://www.pine64.org/pinebook/"
description: "The Pinebook is an affordable ARM-based linux laptop. An experimental Ubuntu Touch image is available."
---
